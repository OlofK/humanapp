package human;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class Human {

  @GeneratedValue
  @Id
  private long id;

  @NotNull
  private String human;

  private String uuid = UUID.randomUUID().toString();

  public long getId() {
    return this.id;
  }

  public String getHuman() {
    return this.human;
  }

  public void setHuman(String human) {
    this.human = human;
  }

  public String getUUID() {
    return this.uuid;
  }
}
