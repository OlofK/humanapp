package human;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.Collection;

@Stateless
public class HumanService {

  @PersistenceContext(unitName = "humanapp")
  private EntityManager entityManager;

  public String insertHuman(String humanName) {
    Human humanEntity = new Human();
    humanEntity.setHuman(humanName);
    entityManager.persist(humanEntity);

    return "Human " + humanName + " has been incorporated into database.";
  }

  public String allHumans() {
    Query q = entityManager.createQuery("select h from Human h");
    Collection<Human> humanCollection = q.getResultList();

    if (humanCollection.isEmpty()) {
      return "No humans in database";
    }

    String humanString = "";
    for (Human human : humanCollection) {
      humanString += "{" + human.getHuman() + ": " + human.getUUID() + "}\n";
    }
    return humanString;
  }

  public String findHuman(String humanName) {
    Query q = entityManager.createQuery("select h from Human h where human= :humanName");
    q.setParameter("humanName", humanName);
    Collection<Human> humanCollection = q.getResultList();

    if (humanCollection.isEmpty()) {
      return "Human " + humanName + " not found";
    }

    String humanString = "Humans found:\n";
    for (Human h : humanCollection) {
      humanString += h.getHuman() + ", " + h.getUUID() + "\n";
    }
    return humanString;
  }
}
