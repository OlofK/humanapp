package human;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;

@WebServlet("")
public class HumanServlet extends HttpServlet {

  @Inject
  private HumanService humanService;
  private String str, humans;

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {

    String human = request.getParameter("human");
    String message = humanService.insertHuman(human);

    response.setHeader("Access-Control-Allow-Origin", "*");
    response.getWriter().println(message);
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    response.setHeader("Access-Control-Allow-Origin", "*");

    if (!request.getParameterNames().hasMoreElements()) {
      humans = humanService.allHumans();
      response.getWriter().println(humans);
    } else if (request.getParameterMap().containsKey("human")) {
      String name = request.getParameter("human");
      String message = humanService.findHuman(name);
      response.getWriter().println(message);
    } else {
      response.getWriter().println("Unknown action");
    }
  }

  @Override
  protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
    response.setHeader("Access-Control-Allow-Origin", "*");
  }
}
